import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import axios from 'axios'

class App extends Component {

  constructor () {
    super()
    this.state = {
      username: ''
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick () {
      axios.get('/test')
        .then(response => console.log(response));
        //.then(response => this.setState({username: response.data}))
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <button className='button' onClick={this.handleClick}>Click Me</button>
        <p className='display'>{this.state.username}</p>
      </div>
    );
  }
}

export default App;
