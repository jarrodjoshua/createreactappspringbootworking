package com.npmstart.createreact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreatereactApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreatereactApplication.class, args);
	}
}
